<div class="progress-bar flex gap-1 items-center mx-auto justify-center mt-6">
    <div class="bg-[#F3F4F6] step step0 w-[70.15px] h-[6px] rounded"></div>
    <div class="bg-[#F3F4F6] step step1 w-[70.15px] h-[6px] rounded"></div>
    <div class="bg-[#F3F4F6] step step2 w-[70.15px] h-[6px] rounded"></div>
    <div class="bg-[#F3F4F6] step step2 w-[70.15px] h-[6px] rounded"></div>
    <div class="bg-[#F3F4F6] step step2 w-[70.15px] h-[6px] rounded"></div>
    <div class="bg-[#F3F4F6] step step2 w-[70.15px] h-[6px] rounded"></div>
    <div class="bg-[#F3F4F6] step step2 w-[70.15px] h-[6px] rounded"></div>
    <div class="bg-[#F3F4F6] step step2 w-[70.15px] h-[6px] rounded"></div>
</div>
@yield('progres')
