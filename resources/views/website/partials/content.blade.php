<div>
    @include('website.partials.navbar')

    <div class=" min-h-[90vh]">
        @yield('content')
    </div>

    @include('website.partials.footer')
</div>
